package com.example.lynn.game;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static com.example.lynn.game.MainActivity.*;

/**
 * Created by lynn on 6/9/2016.
 */
public class GameView extends RelativeLayout {

    public GameView(Context context) {
        super(context);

        images = new ImageView[10][10];

        int id = 1;

        drawable = getResources().getDrawable(R.drawable.puzle);

        blank = getResources().getDrawable(R.drawable.blank);

        for (int counter=0;counter<images.length;counter++)
            for (int counter1=0;counter1<images[counter].length;counter1++) {
                images[counter][counter1] = new ImageView(context);

                images[counter][counter1].setImageDrawable(blank);

                images[counter][counter1].setId(id++);

                images[counter][counter1].setOnClickListener(listener);

                images[counter][counter1].setTag(counter + ";" + counter1);
            }

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(100,100);

        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP,getId());

        images[0][0].setLayoutParams(layoutParams);

        for (int counter=0;counter<images.length;counter++)
            for (int counter1=0;counter1<images[0].length;counter1++) {
                layoutParams = new RelativeLayout.LayoutParams(100,100);

                if (counter1 > 0)
                    layoutParams.addRule(RelativeLayout.RIGHT_OF,images[counter][counter1-1].getId());

                if (counter > 0)
                    layoutParams.addRule(RelativeLayout.BELOW,images[counter-1][counter1].getId());

                images[counter][counter1].setLayoutParams(layoutParams);
            }

        for (int counter=0;counter<images.length;counter++)
            for (int counter1=0;counter1<images[0].length;counter1++)
                addView(images[counter][counter1]);

        score = new TextView(context);

        layoutParams = new RelativeLayout.LayoutParams(100,100);

        layoutParams.addRule(RelativeLayout.BELOW,images[images.length-1][0].getId());

        score.setLayoutParams(layoutParams);

        score.setText(String.valueOf(theScore));

        addView(score);

        new MyThread();
    }

}
