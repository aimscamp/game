package com.example.lynn.game;

import static com.example.lynn.game.MainActivity.*;

/**
 * Created by lynn on 6/9/2016.
 */
public class MyThread implements Runnable {
    private Thread thread;
    private boolean keepGoing;

    public MyThread() {
        thread = new Thread(this);

        keepGoing = true;

        thread.start();
    }

    public void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    @Override
    public void run() {
        while (keepGoing) {
            final int row = (int)(images.length*Math.random());
            final int column = (int)(images.length*Math.random());

            double time = 2*Math.random();

            images[row][column].post(new Runnable() {

                @Override
                public void run() {
                    images[row][column].setImageDrawable(drawable);

                    visible = true;

                    selectedRow = row;
                    selectedColumn = column;
                }

            });

            pause(time);


            images[row][column].post(new Runnable() {

                @Override
                public void run() {
                    images[row][column].setImageDrawable(blank);

                    visible = false;

                    selectedRow = -1;
                    selectedColumn = -1;
                }

            });

            pause(1);
        }

    }

}
