package com.example.lynn.game;

import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import static com.example.lynn.game.MainActivity.*;

/**
 * Created by lynn on 6/9/2016.
 */
public class MyListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        if (v instanceof ImageView) {
            ImageView source = (ImageView)v;

            String[] tokens = source.getTag().toString().split(";");

            int row = Integer.parseInt(tokens[0]);
            int column = Integer.parseInt(tokens[1]);

            if (visible &&
                    (row == selectedRow) &&
                    (column == selectedColumn)) {
                theScore += 10;

                score.setText(String.valueOf(theScore));
            }



        }

    }

}
